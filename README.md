# White Noise Mobian

Basic white noise app for Mobian

All icons were downloaded from [flaticon](https://www.flaticon.com/packs/music-65?k=1609280718770) thanks to [freepik](https://www.flaticon.com/authors/freepik)!

![Screenshot](/screenshot/white_noise_screenshot.png)


## Bugs/Issues

 - Sometimes when closing the app it doesn't stope the noise.
 - The volume slider only seems to work to turn the volume on and off


## Dependencies
This app requires pythons `pygame` which can be installed with `sudo apt install python-pygame` and then `pip3 install pygame`. 


## Installation
 - Clone this repo with `https://gitlab.com/greenbeast/white_noise_mobian.git`
 - install via the deb by cd'ing into the directory and running `sudo apt install ./white-noise-mobian_0.9_all.deb`
 OR
 - `cd white_noise_mobian`
 - `python3 icon_install.py`
 OR
 - `cd white_noise_mobian`
 - `./install.sh`

## license

MIT

## Wishlist
 - Would like to make icons for each type of noise and my own app icon but I honestly have no clue how to do that...


