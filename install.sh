#!/usr/bin/bash

# pulled from https://github.com/joanisc/loyaltyCardsOpen/blob/master/install.sh
# which is a project I contribute to

path="${HOME}/.local/share"

mkdir -p "$path/white_noise_mobian"

if [ $? -ne 0 ]; then 
	echo ""
	echo "Could not create folder '$path/white_noise_mobian'"
	exit
fi

echo "" 
echo "Copying download folder to $path/white_noise_mobian"
cp -ru ./* "$path/white_noise_mobian"

if [ $? -ne 0 ]; then
	echo "" 
	echo "Could not copy download folder to $path/white_noise_mobian"
	exit
fi

echo "" 
echo "Create a new desktop file"
rm -f "$path/applications/white-noise.desktop"

echo "[Desktop Entry]
Name=White Noise
X-GNOME-FullName=White Noise
Comment=White noise app built for Phosh and tested on Mobian
Keywords=Audio;
Exec="$path/white_noise_mobian/white_noise.sh"
Terminal=false
StartupNotify=true
Type=Application
Icon=audio-x-generic
Categories=GNOME;GTK;Utility;
MimeType=application/python;
Name[en_US]=White Noise
X-Purism-FormFactor=Workstation;Mobile;" > "$path/applications/white-noise.desktop"

if [ $? -ne 0 ]; then 
	echo "" 
	echo "Could not create desktop file in to $path/applications"
	exit
fi

rm -f "$path/white_noise.sh"

echo "#!/usr/bin/bash
cd '$path/white_noise'
python3 white_noise.py" > "$path/white_noise_mobian/white_noise.sh"

chmod 0755 "$path/white_noise_mobian/white_noise.sh"

if [ $? -ne 0 ]; then 
	echo "" 
	echo "Could not create shell script in to $path/white_noise_mobian"
	exit
fi

echo "" 
echo "Install complete - you may (optionally) remove the download folder"
