#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import subprocess as sp

username = sp.check_output('whoami', shell=True)
username = username.decode('utf-8').strip('\n')

if isinstance(username, list):
    username = username[0]
else:
    username = username

script_path = os.path.dirname(os.path.realpath(__file__))
path = f"{script_path}/white_noise.py"
install_path = f"/home/{username}/.local/share/applications/white_noise.desktop"

icon_file = f"""[Desktop Entry]
Name=White Noise
Comment=White noise player for Phosh on the PinePhone
Type=Application
Icon=audio-x-generic
Exec={path}
Categories=GTK;Audio;Player;
X-Purism-FormFactor=Workstation;Mobile;
"""

with open(install_path, "w+") as f:
    f.write(icon_file)
